package test;

import com.oliver.arguparse.annotation.BooleanArgument;
import com.oliver.arguparse.annotation.DoubleArgument;
import com.oliver.arguparse.annotation.NumberArgument;
import com.oliver.arguparse.annotation.TextArgument;
import com.oliver.arguparse.parse.AnnotationParser;

public class Test {
	
	@BooleanArgument(name = "enabled", def = false, desc = "Explicitly enables the flux capacitor")
	@TextArgument(name = "flux_name", def = "Flux capacitor", desc = "Gives the flux capacitor a nice friendly name")
	@TextArgument(name = "flux_nickname", def = "Fluxxy", desc = "Gives the flux capacitor an easy to remember nickname")
	@NumberArgument(name = "flux_runtume", def = 10L, min = 1L, max = 60L, desc = "How long the flux capacitor will run for when enabled (in seconds)")
	@DoubleArgument(name = "granularity", def = 0.1d, min = 0.0d, max = 100.0d, desc = "How much detail the flux capacitor will go into")
	public static void main(String... args) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		System.out.println(AnnotationParser.processArgs(new String[] { "-enabled", "-h" }));
	}
}
