package com.oliver.arguparse.parse;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import com.oliver.arguparse.annotation.BooleanArgument;
import com.oliver.arguparse.annotation.DoubleArgument;
import com.oliver.arguparse.annotation.NumberArgument;
import com.oliver.arguparse.annotation.TextArgument;
import com.oliver.arguparse.annotation.multi.BooleanArguments;
import com.oliver.arguparse.annotation.multi.DoubleArguments;
import com.oliver.arguparse.annotation.multi.NumberArguments;
import com.oliver.arguparse.annotation.multi.TextArguments;

public class AnnotationParser {

	private static Annotation[] getAnnotations() throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		Method   m = null;
		Class<?> callerClass;
		callerClass = getCallerClass();
		m           = callerClass.getMethod(Thread.currentThread().getStackTrace()[3].getMethodName(), String[].class);
		return m.getAnnotations();
	}

	public static String processArgs(String[] args) {
		Annotation[] as;
		try {
			as = getAnnotations();
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
			return "Something went wrong whule looking for your class";
		}

		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if ("-h".equalsIgnoreCase(arg) || "-help".equalsIgnoreCase(arg)) {
				return printHelp(as);
			}
		}
		return "";
	}


	private static String printHelp(Annotation[] as) {
		StringBuilder sb = new StringBuilder();
		sb.append(formatTableHeader());
		for (Annotation a : as) {
			if (a instanceof BooleanArgument) {
				sb.append(booleanHelp((BooleanArgument) a));
			} else if (a instanceof DoubleArgument) {
				sb.append(doubleHelp((DoubleArgument) a));
			} else if (a instanceof NumberArgument) {
				sb.append(numberHelp((NumberArgument) a));
			} else if (a instanceof TextArgument) {
				sb.append(textHelp((TextArgument) a));
			} else if (a instanceof BooleanArguments) {
				sb.append(booleansHelp((BooleanArguments) a));
			} else if (a instanceof DoubleArguments) {
				sb.append(doublesHelp((DoubleArguments) a));
			} else if (a instanceof NumberArguments) {
				sb.append(numbersHelp((NumberArguments) a));
			} else if (a instanceof TextArguments) {
				sb.append(textsHelp((TextArguments) a));
			}
		}
		return sb.toString();
	}

	private static final int NAME_COL        = 20;
	private static final int DEFAULT_COL     = 20;
	private static final int MIN_COL         = 10;
	private static final int MAX_COL         = 10;
	private static final int DESCRIPTION_COL = 50;

	private static String formatTableHeader() {
		return formatAsTable(
				centerAlign("Name", NAME_COL),
				centerAlign("Default", DEFAULT_COL),
				centerAlign("Min", MIN_COL),
				centerAlign("Max", MAX_COL),
				leftAlign("Description", DESCRIPTION_COL));
	}

	private static String formatAsTable(String name, String def, String min, String max, String desc) {
		return name + "|" + def + "|" + min + "|" + max + "|" + desc + "\n";
	}

	private static String centerAlign(String str, int width) {
		if (str == null) {
			str = "";
		}
		if (str.length() >= width) {
			return str.substring(0, width);
		}
		int halfWord  = str.length() / 2;
		int halfWordRemainder = halfWord * 2 == str.length() ? 0 : 1;
		int halfWidth = (width / 2) - halfWord;
		int halfWidthRemainder = (width/2)*2 == width ? 0 : 1;

		str = padBeginning(str, halfWidth);
		str = padEnd(str, (halfWidth - halfWordRemainder) + halfWidthRemainder);
		return str;
	}

	private static String leftAlign(String str, int width) {
		if (str == null) {
			str = "";
		}
		if (str.length() >= width) {
			return str.substring(0, width);
		}
		return padEnd(str, width - str.length());
	}

	private static String rightAlign(String str, int width) {
		if (str == null) {
			str = "";
		}
		if (str.length() >= width) {
			return str.substring(0, width);
		}
		return padBeginning(str, width - str.length());
	}

	private static String padBeginning(String str, int pad) {
		while (pad-- > 0) {
			str = " " + str;
		}
		return str;
	}

	private static String padEnd(String str, int pad) {
		while (pad-- > 0) {
			str += " ";
		}
		return str;
	}

	private static String booleanHelp(BooleanArgument b) {
		return formatAsTable(
				leftAlign("-" + b.name(), NAME_COL),
				rightAlign(b.def() + "", DEFAULT_COL),
				rightAlign(null, MIN_COL),
				rightAlign(null, MAX_COL),
				leftAlign(b.desc() + "", DESCRIPTION_COL));
	}

	private static String doubleHelp(DoubleArgument d) {
		return formatAsTable(
				leftAlign("-" + d.name(), NAME_COL),
				rightAlign(d.def() + "", DEFAULT_COL),
				rightAlign(d.min() + "", MIN_COL),
				rightAlign(d.max() + "", MAX_COL),
				leftAlign(d.desc() + "", DESCRIPTION_COL)
				);
	}

	private static String numberHelp(NumberArgument n) {
		return formatAsTable(
				leftAlign("-" + n.name(), NAME_COL),
				rightAlign(n.def() + "", DEFAULT_COL),
				rightAlign(n.min() + "", MIN_COL),
				rightAlign(n.max() + "", MAX_COL),
				leftAlign(n.desc() + "", DESCRIPTION_COL));
	}

	private static String textHelp(TextArgument t) {
		return formatAsTable(
				leftAlign("-" + t.name(), NAME_COL),
				rightAlign(t.def() + "", DEFAULT_COL),
				rightAlign(null, MIN_COL),
				rightAlign(null, MAX_COL),
				leftAlign(t.desc() + "", DESCRIPTION_COL));
	}

	private static String booleansHelp(BooleanArguments bs) {
		StringBuilder sb = new StringBuilder();
		for (BooleanArgument b : bs.value()) {
			sb.append(booleanHelp(b));
		}
		return sb.toString();
	}

	private static String doublesHelp(DoubleArguments ds) {
		StringBuilder sb = new StringBuilder();
		for (DoubleArgument d : ds.value()) {
			sb.append(doubleHelp(d));
		}
		return sb.toString();
	}

	private static String numbersHelp(NumberArguments ns) {
		StringBuilder sb = new StringBuilder();
		for (NumberArgument n : ns.value()) {
			sb.append(numberHelp(n));
		}
		return sb.toString();
	}

	private static String textsHelp(TextArguments ts) {
		StringBuilder sb = new StringBuilder();
		for (TextArgument t : ts.value()) {
			sb.append(textHelp(t));
		}
		return sb.toString();
	}

	private static Class<?> getCallerClass() throws ClassNotFoundException {
		final StackTraceElement[] elements = new Throwable().getStackTrace();
		for (final StackTraceElement element : elements) {
			if (isValidlMethod(element)) {
				if (!element.getClassName().equals(AnnotationParser.class.getCanonicalName())) {
					return Class.forName(element.getClassName());
				}
			}
		}
		return null;
	}

	private static boolean isValidlMethod(final StackTraceElement element) {
		if (element.isNativeMethod()) {
			return false;
		}
		final String cn = element.getClassName();
		if (cn.startsWith("sun.reflect.")) {
			return false;
		}
		final String mn = element.getMethodName();
		if (cn.startsWith("java.lang.reflect.") && (mn.equals("invoke") || mn.equals("newInstance"))) {
			return false;
		}
		if (cn.equals("java.lang.Class") && mn.equals("newInstance")) {
			return false;
		}
		return true;
	}

}
