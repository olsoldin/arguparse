package com.oliver.arguparse.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.oliver.arguparse.annotation.multi.BooleanArguments;

/**
 * eg.
 * <pre>
 * {@code java -jar Argument.jar -name}
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(value=BooleanArguments.class)
public @interface BooleanArgument{
	public String name() default "";
	public boolean def() default false;
	public String desc() default "";
}
