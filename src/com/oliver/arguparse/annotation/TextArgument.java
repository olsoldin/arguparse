package com.oliver.arguparse.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.oliver.arguparse.annotation.multi.TextArguments;

/**
 * eg.
 * <pre>
 * {@code java -jar Argument.jar -name "Text goes here"}
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(value=TextArguments.class)
public @interface TextArgument {
	public String name() default "";
	public String def() default "";
	public String desc() default "";
}
