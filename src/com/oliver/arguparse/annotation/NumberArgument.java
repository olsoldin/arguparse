package com.oliver.arguparse.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.oliver.arguparse.annotation.multi.NumberArguments;

/**
 * eg.
 * <pre>
 * {@code java -jar Argument.jar -name 50}
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(value=NumberArguments.class)
public @interface NumberArgument {
	public String name() default "";
	public long def() default 0L;
	public long min() default Long.MIN_VALUE;
	public long max() default Long.MAX_VALUE;
	public String desc() default "";
}
