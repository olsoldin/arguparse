package com.oliver.arguparse.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.oliver.arguparse.annotation.multi.DoubleArguments;

/**
 * eg.
 * <pre>
 * {@code java -jar Argument.jar -name 50.0}
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(value=DoubleArguments.class)
public @interface DoubleArgument {
	public String name() default "";
	public double def() default 0d;
	public double min() default Double.MIN_VALUE;
	public double max() default Double.MAX_VALUE;
	public String desc() default "";
}
