package com.oliver.arguparse.annotation.multi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.oliver.arguparse.annotation.DoubleArgument;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DoubleArguments {
	DoubleArgument[] value();
}
